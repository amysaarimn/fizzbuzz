
public class FizzBuzz {
	
	public static void main(String args[]) {
		FizzBuzz fb = new FizzBuzz();
		fb.run();
	}
    
	public void run() {
		
		for( int i = 1; i < 101; i++ ) {
			if( i%3 == 0  &&  i%5 == 0 ) {
				printBoth();
			} else if ( i%3 == 0 ) {
				printFizz();
			} else if ( i%5 == 0 ) {
				printBuzz();
			} else {
				printNumber(i);
			}
		}
		
	}
	
	protected void printFizz () {
		System.out.println("Fizz");
	}
	
	protected void printBuzz () {
		System.out.println("Buzz");
	}
	
	protected void printBoth() {
		System.out.println("FizzBuzz");
	}
	
	protected void printNumber( int i ) {
		System.out.println( i ) ;
	}
}
