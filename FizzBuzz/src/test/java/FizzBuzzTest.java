import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;



public class FizzBuzzTest {
	
	/*
	 * Write a program that prints the numbers from 1 to 100. But for multiples of three print �Fizz� 
	 * instead of the number and for the multiples of five print �Buzz�. 
	 * For numbers which are multiples of both three and five print �FizzBuzz�.
	 */

	
    @Test 
    public void fizzbuzz_success() {
        // set up
    	FizzBuzz spy = PowerMockito.spy(new FizzBuzz());
    	
    	// run test
    	spy.run();
    	
    	// verify results
    	verify(spy, Mockito.times(27)).printFizz();
    	verify(spy, Mockito.times(14)).printBuzz();
    	verify(spy, Mockito.times(6)).printBoth();
    	verify(spy, Mockito.times(53)).printNumber(anyInt());
        
    }
    
}
